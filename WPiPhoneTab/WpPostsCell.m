//
//  WpPostsCell.m
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "WpPostsCell.h"
#import "WpSettings.h"
#import "UIImageView+WebCache.h"
#import "NSString+HTML.h"
@implementation WpPostsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIView * c =[[[NSBundle mainBundle] loadNibNamed:@"WpPostsCell" owner:self options:nil] objectAtIndex:0];
        c.opaque = YES;
        c.backgroundColor = [UIColor clearColor];
        
        [self.contentView addSubview:c];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithPost:(Post * )p {
    
    self.title.text = [p.title stringByDecodingHTMLEntities];
    [self.title setFont:WPPOSTCELL_TITLE_FONT];
    self.title.textColor = WPPOSTCELL_TITLE_COLOR;
    
    self.description.text = [[p.content stringByConvertingHTMLToPlainText] stringByDecodingHTMLEntities];
    [self.description setFont:WPPOSTCELL_DESCRIPTION_FONT];
    
    [self.thumbnail setImageWithURL:[NSURL URLWithString:[p.thumbnail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
}

@end
