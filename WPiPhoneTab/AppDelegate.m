//
//  AppDelegate.m
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "AppDelegate.h"
#import "Wordpress.h"
#import "WpSettings.h"
#import "HandsomeManager.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    self.tabBar = [[UITabBarController alloc] init];
    [[HandsomeManager shared] setProjectId:@"ff8080813d7cfe90013d7d150d8f0001"];
    
    GetCategoriesRequest * req = [[GetCategoriesRequest alloc]  initWithDelegate:self];
    req.blogName = BLOGNAME;

    req.successBlock = ^{
        GetCategoriesResponse * resp = (GetCategoriesResponse* )[[GetCategoriesResponse alloc] init];
        resp = req.response;
        NSMutableArray * categories = [[NSMutableArray alloc] initWithCapacity:50];
        [categories addObjectsFromArray:resp.categories];
        NSMutableArray *navs = [[NSMutableArray alloc] init];
        for(WPCategory * cat in categories){
            if(cat.postCount != 0) {
                WpCategoriesViewController  *vc = [[WpCategoriesViewController alloc] initWithNibName:@"WpCategoriesViewController" bundle:nil];
                vc.category = cat.slug;
                vc.headerTitle = cat.name;
                UINavigationController *homeNav = [[UINavigationController alloc] initWithRootViewController:vc];
                [navs addObject:homeNav];
                homeNav.tabBarItem.title = cat.name;
            }
        }
        WPHomeViewController * homeVc = [[WPHomeViewController alloc] initWithNibName:@"WPHomeViewController" bundle:nil];
        UINavigationController *homeNav = [[UINavigationController alloc] initWithRootViewController:homeVc];
        homeNav.tabBarItem.title = @"Ana Sayfa";
        [navs insertObject:homeNav atIndex:0];
        self.tabBar.viewControllers = navs;
        self.window.rootViewController = self.tabBar;
        [self.window makeKeyAndVisible];
        
    };
    
    [req run];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
