//
//  WpBaseViewController.h
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//
#import "RequestManager.h"
#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "WpPostsViewController.h"
#import "Wordpress.h"
#import "HandsomeManager.h"
#import "WpPostsCell.h"
#import "SliderCV.h"
#import "WpSettings.h"
@interface WpBaseViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, HandsomeRequestDelegate, EGORefreshTableHeaderDelegate>
{
      IBOutlet UIActivityIndicatorView *indicator;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UIScrollView * scrollView;
@property (nonatomic, retain) IBOutlet NSMutableArray *dataArr;
@property (assign) NSInteger page;
@property (assign) NSInteger pageCount;
@property (nonatomic, retain) EGORefreshTableHeaderView *_refreshHeaderView;
@property (assign) BOOL _reloading;
@property (assign) BOOL willContinue;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * headerTitle;
@property (nonatomic, retain) NSString * token;
@property (nonatomic, retain) NSString * richMediaToken;
@property (nonatomic, retain) IBOutlet UIAlertView *alert;
@property (nonatomic, retain) IBOutlet UIView *loadingView;
-(void)configureCell:(WpPostsCell*)cell params:(Post *) n;
//-(void)configureSliderCell:(SliderCV*)cell withParams:(NSArray *) arr andDelegate:(id) delegate;
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view;
- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view;
- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view;
-(UITableViewCell*)cellforIndex:(NSIndexPath*)indexPath withReuse:(NSString*)reuse;
-(UITableViewCell*)sliderCellforIndex:(NSIndexPath*)indexPath withReuse:(NSString*)reuse;
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) tappedOnSlider:(int) index ;
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
-(void)hideIndicators;
-(void)showIndicators;


@end
