//
//  DealCellView.m
//  PSM
//
//  Created by Anil Can Baykal on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SliderCV.h"
#import "Wordpress.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@implementation SlideView
@synthesize index;
@synthesize delegate;
@synthesize imageView;
@synthesize selectedImageView;
@synthesize isSelected;
@synthesize isSelectable;
@synthesize lblTitle;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        
        imageView = [[UIImageView alloc] initWithFrame:frame];
		[self addSubview:imageView];

        selectedImageView.hidden = YES;
        
        isSelected = NO;
        isSelectable = NO;
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.frame.size.height - 64, 320, 64)];
        lblTitle.alpha = 0.6;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        lblTitle.backgroundColor = [UIColor blackColor];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
             lblTitle.textAlignment =  NSTextAlignmentCenter;
        }else{
             lblTitle.textAlignment = UITextAlignmentCenter;
        }
       
        lblTitle.numberOfLines = 3;
        
        [self addSubview:lblTitle];
        
    }
    return self;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(tappedOnSlider:)])
        [self.delegate tappedOnSlider:self.index];
    
}

- (void)dealloc {
	[delegate release];
	[imageView release];
    [selectedImageView release];
    [lblTitle release];
    [super dealloc];
}


@end



@implementation SliderCV
@synthesize dataArr;
@synthesize scroll;
@synthesize pControl;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
        UIView * c =[[[NSBundle mainBundle] loadNibNamed:@"SliderCV" owner:self options:nil] objectAtIndex:0];
        c.opaque = YES; 
        c.backgroundColor = [UIColor clearColor];                 
        
        [self addSubview:c];
        
        self.dataArr = [[NSMutableArray alloc] init];
        
    }
            
    return self;
}

- (void) update:(NSArray *) arr andDelegate:(id) dlg {
    self.delegate = dlg;

    [self.dataArr removeAllObjects];
    [self.dataArr addObjectsFromArray:arr];
    
    //NSLog(@"%@", arr);
    //NSLog(@"%d", [arr count]);
    
    for (UIView *v in self.scroll.subviews) {
        [v removeFromSuperview];
    }
    
    int i = 0;
    for (Post *p in arr) {
        UIView *v  = [[UIView alloc] initWithFrame:CGRectMake(i * 320, 0, 320, 187)];
        SlideView *sv = [[SlideView alloc] initWithFrame:CGRectMake(0, 0, 320, 187)];
        sv.delegate = self.delegate;
        sv.index = i;
        //NSLog(@"url%@",url);

//        const char * utf8string = [[NSString stringWithFormat:@"%@", p.thumbnail] UTF8String];
//        NSLog(@"mcyz:%@",[NSString stringWithUTF8String:utf8string]);
        [sv.imageView setImageWithURL:[NSURL URLWithString:[p.thumbnail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        sv.lblTitle.text = p.title;
        //sv.imageView.backgroundColor = [UIColor redColor];
        //sv.imageView.hidden = NO;
        [v addSubview:sv];
        [self.scroll addSubview:v];
        [v release];
        [sv release];

        i++;
    }
    
    [pControl removeFromSuperview];
    pControl = [[DDPageControl alloc] init];
    //[pControl setCenter: CGPointMake(self.contentView.center.x, 315.0f)];
    [pControl setCenter: CGPointMake(160.0f, 175.0f)];
    [pControl setNumberOfPages:[dataArr count]];
    [pControl setCurrentPage:0];
    [pControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged] ;
    [pControl setDefersCurrentPageDisplay: YES] ;
    [pControl setType: DDPageControlTypeOnFullOffFull] ;
    [pControl setOnColor: [UIColor lightGrayColor]] ;
    [pControl setOffColor: [UIColor darkGrayColor]] ;
    [pControl setIndicatorDiameter: 10.0f] ;
    [pControl setIndicatorSpace: 10.0f] ;
    [self addSubview:pControl] ;
    //NSLog(@"%d", [scroll.subviews count]);
    scroll.contentSize = CGSizeMake([scroll.subviews count] * 320, 187);
    
}

- (void)pageControlClicked:(id)sender
{
    DDPageControl *thePageControl = (DDPageControl *)sender ;
    [scroll setContentOffset: CGPointMake(scroll.bounds.size.width * thePageControl.currentPage, scroll.contentOffset.y) animated:YES];
}

#pragma mark -
#pragma mark UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGFloat pageWidth = scroll.bounds.size.width ;
    float fractionalPage = scroll.contentOffset.x / pageWidth ;
    NSInteger nearestNumber = lround(fractionalPage) ;
    
    if (pControl.currentPage != nearestNumber)
    {
        pControl.currentPage = nearestNumber ;
        
        if (scroll.dragging)
            [pControl updateCurrentPageDisplay] ;
    }
    [self.timer invalidate];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
    [pControl updateCurrentPageDisplay] ;
    [self startTimer];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
     [self startTimer];
}
- (IBAction)prevNext:(id)sender {
    
    if ([sender tag] == 1) {
        if ((pControl.currentPage - 1) >= 0)
        [scroll setContentOffset: CGPointMake(scroll.bounds.size.width * (pControl.currentPage - 1), scroll.contentOffset.y) animated:YES];
        
    } else {
        if ((pControl.currentPage + 1) <= [dataArr count] - 1)
        [scroll setContentOffset: CGPointMake(scroll.bounds.size.width * (pControl.currentPage + 1), scroll.contentOffset.y) animated:YES];
    }
    
}

- (void)dealloc {
    [pControl release];
    [dataArr release];
    [scroll release];
    [super dealloc];
}

- (void) onTimer  {
    
    // Updates the variable h, adding 100 (put your own value here!)
    // h += 100;
    
    //This makes the scrollView scroll to the desired position
    //  if(i==10){
    //    i=0;
    // [slider.scroll setContentOffset:CGPointMake(768*i,0 ) animated:NO];
    // }
    
    // content size-1 * 320
    // TODO: burayı contente göre otomatik ayarla !
    if(self.scroll.contentOffset.x == 1280){
        [self.scroll setContentOffset:CGPointMake(0,0 ) animated:YES];
    }
    else if((int)self.scroll.contentOffset.x % 320 == 0 && self.scroll.contentOffset.x != 1280){
        [self.scroll setContentOffset:CGPointMake(self.scroll.contentOffset.x+320,0 ) animated:YES];
        
    }
    //[slider.scroll setContentOffset:CGPointMake(768*i,0 ) animated:YES];
    //[slider.pControl  ]  ;
    //i++;
    
}

-(void) startTimer {
    self.timer=  [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
}

@end
