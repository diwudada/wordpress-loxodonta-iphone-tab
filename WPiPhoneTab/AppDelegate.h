//
//  AppDelegate.h
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestManager.h"
#import "WpCategoriesViewController.h"
#import "WPHomeViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, HandsomeRequestDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) WpCategoriesViewController *viewController;
@property (strong, nonatomic) UITabBarController *tabBar;
@end
