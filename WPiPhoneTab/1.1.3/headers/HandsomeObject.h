//
//  HandsomeObject.h
//
//  Created by Anil Can Baykal on 3/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HandsomeObject : NSObject{
    
    NSString * objectKey;
    NSString * createDate;
    NSString * updateDate;
    
    void (^_saveSuccessBlock)();
}

@property (nonatomic, copy)  NSString * objectKey;

-(void)stroreInUserDefaults:(NSString *)key;
+(HandsomeObject*)loadFromUserDefaults:(NSString *)key;

@end
