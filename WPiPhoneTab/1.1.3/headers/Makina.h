//
//  Makina.h
//  HandsomeLibrary
//
//  Created by Anil Can Baykal on 12/26/12.
//
//

#ifndef HandsomeLibrary_Makina_h
#define HandsomeLibrary_Makina_h


#import "HandsomeManager.h"
#import "Debug.h"
#import "RequestManager.h"

#endif


//==============================================================================
#pragma mark MAKINA
@interface Makina : NSObject{
    
}

+(void)setOpenUdId:(NSString *)openUdId;

+(void)launch:(NSString *)applicationToken;
+(void)launch:(NSString *)applicationToken withDispatchTime:(int)eventDispatchTime;
+(void)ping;

@end