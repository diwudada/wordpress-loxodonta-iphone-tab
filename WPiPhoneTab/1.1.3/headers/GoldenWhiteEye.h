/*

created by anlcan
on 2013-02-05 15:39:19.591390
using /base/data/home/apps/s~sync-server/loxo.365070404888856491/generate.pyc
for golden-white-eye.appspot.com

*/

#import <Foundation/Foundation.h>
#import "HandsomeRequest.h"
#import "HandsomeResponse.h"
#import "HandsomeCommons.h"
#import "HandsomeObject.h"



#pragma mark forward class declaration

@class SaveRequest;
@class ListResponse;
@class SignupRequest;
@class LoginRequest;
@class AddUserInfoRequest;
@class DeleteRequest;
@class RemoveUserInfoRequest;
@class LogoutRequest;
@class SaveResponse;
@class LoginResponse;
@class GetUserInfoResponse;
@class GetUserInfoRequest;
@class SignupResponse;
@class ListRequest;
@class HandsomeUser;
@class UserInfo;
@class QueryFilter;


#pragma mark enum declaration

typedef enum{
	SignupResult_EmailExist = 0,
	SignupResult_Failure = 1,
	SignupResult_Success = 2
} SignupResult;


#pragma mark -
#pragma mark MObjects
typedef enum{
	ConsumerCredType_HANDSOME_ID = 0,
	ConsumerCredType_FACEBOOK = 1,
	ConsumerCredType_TWITTER = 2,
	ConsumerCredType_EMAIL = 3,
	ConsumerCredType_INAPP_USERNAME = 4
} ConsumerCredType;


#pragma mark -
#pragma mark MObjects
typedef enum{
	LoginResult_UserNotFound = 0,
	LoginResult_WrongPassword = 1,
	LoginResult_Success = 2
} LoginResult;


#pragma mark -
#pragma mark MObjects
@interface SaveRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * applicationToken;
@property(nonatomic, retain) NSString * serializedObject;


@end
@interface ListResponse : HandsomeResponse {

}
@property(nonatomic, retain) NSMutableArray * results;


@end
@interface SignupRequest : HandsomeRequest {

}
@property(nonatomic) ConsumerCredType  credType;
@property(nonatomic, retain) NSString * serializedUser;
@property(nonatomic, retain) NSString * key;
@property(nonatomic, retain) NSString * applicationToken;


@end
@interface LoginRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * password;
@property(nonatomic, retain) NSString * userName;
@property(nonatomic, retain) NSString * applicationToken;
@property(nonatomic, retain) NSString * email;


@end
@interface AddUserInfoRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * applicationToken;
@property(nonatomic, retain) UserInfo * info;


@end
@interface DeleteRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * applicationToken;


@end
@interface RemoveUserInfoRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * applicationToken;
@property(nonatomic, retain) NSString * ownerKey;
@property(nonatomic, retain) NSString * name;


@end
@interface LogoutRequest : HandsomeRequest {

}


@end
@interface SaveResponse : HandsomeResponse {

}


@end
@interface LoginResponse : HandsomeResponse {

}
@property(nonatomic) LoginResult  result;
@property(nonatomic, retain) HandsomeUser * loggedInUser;


@end
@interface GetUserInfoResponse : HandsomeResponse {

}
@property(nonatomic, retain) UserInfo * userInfo;


@end
@interface GetUserInfoRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * name;
@property(nonatomic, retain) NSString * applicationToken;
@property(nonatomic, retain) NSString * ownerKey;


@end
@interface SignupResponse : HandsomeResponse {

}
@property(nonatomic, retain) HandsomeUser * signedUpUser;
@property(nonatomic) SignupResult  result;


@end
@interface ListRequest : HandsomeRequest {

}
@property(nonatomic) int  skip;
@property(nonatomic, retain) NSMutableArray * filters;
@property(nonatomic, retain) NSString * applicationToken;
@property(nonatomic) int  limit;


@end
@interface HandsomeUser : HandsomeObject {

}
@property(nonatomic, retain) NSMutableArray * additionalInfo;
@property(nonatomic, retain) NSString * password;
@property(nonatomic, retain) NSString * userID;
@property(nonatomic, retain) NSString * email;
@property(nonatomic, retain) NSString * userName;


@end
@interface UserInfo : HandsomeObject {

}
@property(nonatomic, retain) NSString * ownerKey;
@property(nonatomic, retain) NSString * name;
@property(nonatomic, retain) NSString * value;


@end
@interface QueryFilter : HandsomeObject {

}
@property(nonatomic, retain) NSString * fieldName;
@property(nonatomic, retain) NSString * fieldValue;


@end
