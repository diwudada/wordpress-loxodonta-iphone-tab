//
//  HandsomeManager.h
//  HandsomeLibrary
//
//  Created by Anil Can Baykal on 11/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HandsomeRequest.h"
#import "HandsomeCommons.h"

#import "GoldenWhiteEye.h"
#define kDefaultEventDispatchTime	60 * 1

@class SignupResponse;
@class LoginResponse;

//==============================================================================
typedef void(^HandsomeCallback)();
typedef void(^HandsomeSignupCallback)(SignupResponse*);
typedef void(^HandsomeLoginCallback)(LoginResponse* );
typedef void(^HandsomeUserInfoCallBack)(id);

@class HandsomeUser;
@class EventItem; 

@interface HandsomeUser(UserInfoAddition)

-(void)setObject:(id)object name:(NSString *)name;
-(void)getObject:(NSString *)name completion:(HandsomeUserInfoCallBack)callback;

@end

//==============================================================================
// REGISTER NOTIFICATIONS
#define HandsomeApplicationLaunchSuccess	@"com.nomad.application.success"
#define HandsomeApplicationLaunchFail		@"com.nomad.application.failed"

//==============================================================================
// HANDSOME OBJECT NOTIFICATIONS
#define notificationHandsomeObjectSaved		@"com.nomad.handsome.object.saved"
#define notificationHandsomeObjectUpdated	@"com.nomad.handsome.object.updated"
#define notificationHandsomeObjectDeleted	@"com.nomad.handsome.object.deleted"
#define notificationHandsomeObjectFailed	@"com.nomad.handsome.object.failed"

#define HandsomeTestToken					@"TEST@MAKINA"

//==============================================================================
// HANDSOME EVENTS
#define eventApplicationRegistered		@"HandsomeAppRegistered"
#define eventApplicationRegisterFailed	@"HandsomeAppRegisterFailed"
#define eventApplicationSessionStarted	@"HandsomeAppSessionStarted"
#define eventApplicationSessionEnded	@"HandsomeAppSessionEnded"


#define keyMakinaRegister				@"com.nomad.handsome.key.Registered"

//==============================================================================
//  HANDSOME DATA STORE
#pragma mark HANDSOME DATA STORE

typedef void(^HandsomeUserQueryCallBack)(NSArray*);

@interface HandsomeObject (DataStore)

-(void)save;
-(void)saveWithCallback:(HandsomeCallback)callback;
-(void)saveWithTarget:(id)target andSelector:(SEL)selector;

-(void)remove;
-(void)removeWithTarget:(id)target andSelector:(SEL)selector;

//+(void)listHandsomeObjects:(NSString *)className target:(id)target selector:(SEL)selector;
+(void)listHandsomeObjects:(NSString *)className complete:(HandsomeUserQueryCallBack)callback;
@end


//==============================================================================
@interface HandsomeManager : NSObject<HandsomeRequestDelegate> {
    
    BOOL localOduncu;
    BOOL testRegister;
    
}
@property (nonatomic, copy) NSString *openUdId;

@property (nonatomic, getter= isTracking) BOOL tracking; // event tracking,  enabled by default
@property (nonatomic) int dispatchTime; // tracked event dispatch time, -1 invalidates the dispatcher (though event continued to be tracked)

+(HandsomeManager*)shared;

-(void)setProjectId:(NSString *)projectId;
-(void)setLocalOduncu:(BOOL)isLocal;//this methods has no effect on prod env.
-(void)setTestRegister:(BOOL)isTest;
//==============================================================================
// REGISTER
-(void)launch:(NSString *)applicationToken;
-(void)launch:(NSString *)applicationToken withDispatchTime:(int)eventDispatchTime; 

-(void)pingWithCompletionCallback:(HandsomeCallback)callback;
-(void)ping;

-(NSString *)deviceId;
-(NSString *)applicationToken;

-(void)registerPushToken:(NSData *)deviceToken;


//==============================================================================
// MDATE
-(MDate*)mdateFromDate:(NSDate*)date;
-(MDate*)now;

//==============================================================================
// USER
-(HandsomeUser*)currentUser;
-(void)signupWithEmail:(NSString *)email userName:(NSString *)userName password:(NSString *)password completionCallback:(HandsomeSignupCallback)callback;
-(void)signupWithEmail:(NSString *)email userName:(NSString *)userName facebookID:(NSString *)facebookID completionCallback:(HandsomeSignupCallback)callback;


-(void)loginWithUserName:(NSString *)userName password:(NSString *)password completionCallback:(HandsomeLoginCallback)callback;
-(void)loginWithEmail:(NSString *)userName password:(NSString *)password completionCallback:(HandsomeLoginCallback)callback;


-(void)logoutCurrentUserCompletionCallback:(HandsomeCallback)callback;
-(void)logoutCurrentUser;


//==============================================================================
// EVENT TRACKER

-(void)stopEventTracker;
-(void)resumeEventTracker;

-(int)queueEvent:(EventItem*)event;
-(void)trackEvent:(NSString *)eventName;
-(void)trackEvent:(NSString *)eventName withAction:(NSString *)actionName;
-(void)trackEvent:(NSString *)eventName withValue:(NSString *)valueName forLabel:(NSString *)label;
-(void)trackEvent:(NSString *)eventName withAction:(NSString *)actionName forDuration:(double)duration;

-(void)flushEvents;


//==============================================================================
// REMOTE LOG
-(void)info:(NSString *)msg;
-(void)debug:(NSString *)msg;
-(void)error:(NSString *)msg;


@end
