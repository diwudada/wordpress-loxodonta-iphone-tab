//
//  Request.h
//  Runtime
//
//  Created by Anil Can Baykal on 7/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Marshall.h"
#import "HandsomeObject.h"
#import "HandsomeResponse.h"

@class HandsomeRequest;

//==============================================================================
@protocol HandsomeRequestDelegate <NSObject>

@optional

-(NSDictionary*) additionalHeaders;
-(void)requestWillStart:(HandsomeRequest*)req;
-(void)requestFailed:(HandsomeRequest*)req withError:(id)error;
-(void)requestFinished:(HandsomeRequest*)req withResponse:(HandsomeResponse*)resp;

@end


//==============================================================================
@interface HandsomeRequest : HandsomeObject {
	
    NSString * session; // generated id on every app launch
    NSString * registerId; // first register id, 
	NSString * verb; 	// server side identification
    
@protected
    NSString *host;
    
@private
    //INNER OBJECTS MUST BE PREFIXED "__", otherwise they will be sent to server!
    // new styla calback, easier to write;
    void (^_successBlock)(); 
    void (^_failureBlock)(); 

 	id<HandsomeRequestDelegate> __delegate;
    NSURLConnection * __request;
    int __tag;

}

@property (nonatomic) NSTimeInterval startTime;
@property (nonatomic) NSTimeInterval requestDuration; // time between creation and execution final

@property (nonatomic, copy) NSString *verb;
@property (nonatomic, copy) NSString *session;
@property (nonatomic, copy) NSString *registerId; // given by a Handsome Register Response
@property (nonatomic, copy) NSString *deviceId;   // generated once for device. pseudo-random UDID

@property (nonatomic, assign) id<HandsomeRequestDelegate> delegate;
@property ( nonatomic, assign) NSURLConnection * httpRequest;

@property (nonatomic, copy) void (^failureBlock)();
@property (nonatomic, copy) void (^successBlock)();

@property (nonatomic, retain)HandsomeResponse * response;

-(id)initWithDelegate:(id<HandsomeRequestDelegate>)d;
-(void)run;
-(NSURL*)getHost;
-(void)setEndPointHost:(NSString *)_host;

-(void)cancel;

-(HandsomeResponse*)getHandsomeResponse;


@end



