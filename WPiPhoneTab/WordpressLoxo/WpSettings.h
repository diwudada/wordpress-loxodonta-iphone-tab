//
//  WpSettings.h
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//


#define BLOGNAME @"Zayiflama"


#define TABLEVIEW_HEADER_BACKGROUND_COLOR       [UIColor colorWithRed:0.847 green:0.847 blue:0.847 alpha:1]
#define TABLEVIEW_HEADER_TITLE_COLOR            [UIColor whiteColor]
#define TABLEVIEW_HEADER_TITLE_FONT             [UIFont fontWithName:@"Helvetica-Bold" size:21.0]

#define WPPOSTCELL_TITLE_COLOR                  [UIColor blackColor]
#define WPPOSTCELL_TITLE_FONT                   [UIFont fontWithName:@"Helvetica-Bold" size:16.0]
#define WPPOSTCELL_DESCRIPTION_FONT             [UIFont fontWithName:@"Helvetica" size:14.0]
