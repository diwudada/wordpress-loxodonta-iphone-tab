#import "Wordpress.h"
NSString * const WordpressEndPoint = @"http://toys.loxodonta-editor.appspot.com:80/resources/dispatcher/test/v1/ff8080813d7cfe90013d7d150d8f0001/";


@implementation GetPostsRequest

@synthesize blogName;
@synthesize page;


-(id)initWithDelegate:(id<HandsomeRequestDelegate>)d{

    self = [super initWithDelegate:d];

    if (self) {
        host = WordpressEndPoint;
        

    }
    
    return self;
}

-(void)dealloc{

	[blogName release]; blogName = nil;


    [super dealloc];
}

@end

@implementation GetPostsResponse

@synthesize pageCount;
@synthesize posts;



-(void)dealloc{

	[posts release]; posts = nil;


    [super dealloc];
}

@end
@implementation GetCategoriesRequest

@synthesize blogName;


-(id)initWithDelegate:(id<HandsomeRequestDelegate>)d{

    self = [super initWithDelegate:d];

    if (self) {
        host = WordpressEndPoint;
        

    }
    
    return self;
}

-(void)dealloc{

	[blogName release]; blogName = nil;


    [super dealloc];
}

@end

@implementation GetCategoriesResponse

@synthesize categories;



-(void)dealloc{

	[categories release]; categories = nil;


    [super dealloc];
}

@end
@implementation GetCategoryPostsRequest

@synthesize blogName;
@synthesize categorySlug;
@synthesize page;


-(id)initWithDelegate:(id<HandsomeRequestDelegate>)d{

    self = [super initWithDelegate:d];

    if (self) {
        host = WordpressEndPoint;
        

    }
    
    return self;
}

-(void)dealloc{

	[blogName release]; blogName = nil;
	[categorySlug release]; categorySlug = nil;


    [super dealloc];
}

@end

@implementation GetCategoryPostsResponse

@synthesize posts;
@synthesize pageCount;



-(void)dealloc{

	[posts release]; posts = nil;


    [super dealloc];
}

@end
@implementation Post
@synthesize title;
@synthesize content;
@synthesize author;
@synthesize description;
@synthesize thumbnail;

-(void)dealloc{

	[title release]; title = nil;
	[content release]; content = nil;
	[author release]; author = nil;
	[description release]; description = nil;
    [thumbnail release]; thumbnail = nil;

    [super dealloc];
}

@end
@implementation WPCategory
@synthesize name;
@synthesize postCount;
@synthesize slug;


-(void)dealloc{

	[name release]; name = nil;
	[slug release]; slug = nil;


    [super dealloc];
}

@end
@implementation Blog
@synthesize key;
@synthesize host;


-(void)dealloc{

	[key release]; key = nil;
	[host release]; host = nil;


    [super dealloc];
}

@end