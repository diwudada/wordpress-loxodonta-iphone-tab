/*

created by anlcan
on 2013-03-19 13:19:47.627950
using /base/data/home/apps/s~sync-server/moxo.365948835105045938/generate.pyc
for http://alpha.366044517846823184.loxodonta-editor.appspot.com/

*/

#import <Foundation/Foundation.h>
#import "HandsomeRequest.h"
#import "HandsomeResponse.h"
#import "HandsomeCommons.h"
#import "HandsomeObject.h"



#pragma mark forward class declaration

@class GetPostsRequest;
@class GetPostsResponse;
@class GetCategoriesRequest;
@class GetCategoriesResponse;
@class GetCategoryPostsRequest;
@class GetCategoryPostsResponse;
@class Post;
@class WPCategory;
@class Blog;


#pragma mark enum declaration

@interface GetPostsRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * blogName;
@property(nonatomic) int  page;


@end
@interface GetPostsResponse : HandsomeResponse {

}
@property(nonatomic) int  pageCount;
@property(nonatomic, retain) NSMutableArray * posts;


@end
@interface GetCategoriesRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * blogName;


@end
@interface GetCategoriesResponse : HandsomeResponse {

}
@property(nonatomic, retain) NSMutableArray * categories;


@end
@interface GetCategoryPostsRequest : HandsomeRequest {

}
@property(nonatomic, retain) NSString * blogName;
@property(nonatomic, retain) NSString * categorySlug;
@property(nonatomic) int  page;


@end
@interface GetCategoryPostsResponse : HandsomeResponse {

}
@property(nonatomic, retain) NSMutableArray * posts;
@property(nonatomic) int  pageCount;


@end
@interface Post : HandsomeObject {

}
@property(nonatomic, retain) NSString * title;
@property(nonatomic, retain) NSString * content;
@property(nonatomic, retain) NSString * author;
@property(nonatomic, retain) NSString * description;
@property(nonatomic, retain) NSString * thumbnail;

@end
@interface WPCategory : HandsomeObject {

}
@property(nonatomic, retain) NSString * name;
@property(nonatomic) int  postCount;
@property(nonatomic, retain) NSString * slug;


@end
@interface Blog : HandsomeObject {

}
@property(nonatomic, retain) NSString * key;
@property(nonatomic, retain) NSString * host;


@end
