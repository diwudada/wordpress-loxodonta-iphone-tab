//
//  WPHomeViewController.m
//  WPiPhoneTab
//
//  Created by Mehmet Can Yavuz on 14.05.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "WPHomeViewController.h"
#import "WpPostsViewController.h"
@interface WPHomeViewController ()
{
    int first;
}
@end

@implementation WPHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    first = 0;
    // Do any additional setup after loading the view from its nib.
    self.dataArr = [[NSMutableArray alloc] init];
    self.slider = [[SliderCV alloc] initWithFrame:(CGRectMake(0, 0, 320, 187))];
    if(self._refreshHeaderView == nil){
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
        view.delegate = self;
        [self.tableView addSubview:view];
        self._refreshHeaderView = view;
    }
    [self._refreshHeaderView refreshLastUpdatedDate];
    [self reloadTableViewDataSource];
    self.loadingView.hidden= NO;
    [self.view addSubview:_slider];
}

-(void)viewWillAppear:(BOOL)animated{
    //    AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //    del.stackViewController.delegate = self;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    [super reloadTableViewDataSource];
    [self getPosts];
}

-(void) getPosts {
    self._reloading = YES;
    //    indicatorNav.hidden = NO;
    //    [indicatorNav startAnimating];
    if([self.dataArr count] ==0){
        self.page = 1;
        
    }
    
    GetPostsRequest * request = [[GetPostsRequest alloc] initWithDelegate:self];
    NSString * session = [[RequestManager sharedManager] globalSession];
    request.session = session;
    request.page = self.page;
    request.blogName = BLOGNAME;
    request.successBlock = ^{
        GetCategoryPostsResponse *response = [[GetCategoryPostsResponse alloc] init];
        response = (GetCategoryPostsResponse * )request.response;
        self.pageCount = response.pageCount;
        [self.dataArr addObjectsFromArray:response.posts];
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (int k = 0; k < 5; k++) {
            [arr addObject:[self.dataArr objectAtIndex:k]];
            //MNews *nn = [self.dataArr objectAtIndex:i];
            //NSLog(@"%@", nn.newsId);
        }
        if(self.page==1){
            [_slider update:arr andDelegate:self];
            [_slider.scroll setContentOffset:CGPointMake(0,0 ) animated:YES];
            
        }
        
        if(first ==0){
            [_slider startTimer];
            first=1;
        }
        
        NSLog(@"success");
        NSLog(@"%@",response);
        self._reloading = NO;
        self.tableView.hidden = NO;
        [self hideIndicators];
        [self._refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
        
        
        [self.tableView reloadData];
        
    };
    
    request.failureBlock = ^{
        NSLog(@"fail");
        // InnerFault * response = (InnerFault * )request.response;
        
        [self hideIndicators];
        self._reloading = NO;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Bağlantı Hatası" message:@"Lütfen tekrar deneyiniz" delegate:self cancelButtonTitle:@"Vazgeç" otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Tamam"];
        [alert show];
        
    };
    
    [request run];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row  == self.dataArr.count-6 ){
        if (self.page < self.pageCount && !self._reloading ) {
            self.page += 1;
            [self getPosts];
        }
    }
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSString * reuse = [NSString stringWithFormat:@"Cell_%d", indexPath.row];
    static NSString *reuse = @"GenericCell";
    
    WpPostsCell * cell = [tableView dequeueReusableCellWithIdentifier:reuse];
    
    if ( cell == nil) {
        cell = (WpPostsCell *) [self cellforIndex:indexPath withReuse:(NSString*)reuse];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    Post *n = [self.dataArr objectAtIndex:(indexPath.row+5)];
    [self configureCell:cell params:n];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ([self.dataArr count]-5);
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    [GlobalData sharedData].posts = self.dataArr;
    WpPostsViewController *vc = [[WpPostsViewController alloc] init];
    //    vc.token = self.token;
    //    vc.richMediaToken = self.richMediaToken;
    self.navigationItem.title = @"Geri";
    [self.navigationController pushViewController:vc animated:YES];
    [vc loadPost:[self.dataArr objectAtIndex:(indexPath.row+5)]];
    
    
}

@end
