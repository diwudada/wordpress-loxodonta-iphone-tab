//
//  WpBaseViewController.m
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//
#import "WpPostsViewController.h"
#import "WpBaseViewController.h"
#import "WpSettings.h"
@interface WpBaseViewController ()

@end

@implementation WpBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   // self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yellowMobilLogo.png"]];
    self.tableView.hidden = YES;
    [indicator startAnimating];
    self.willContinue = YES;


    
    
    
}

-(void) viewWillAppear:(BOOL)animated  {
    
        self.navigationItem.title = @"";
   // [GlobalData sharedData].posts = self.dataArr;

    
}



-(void) viewDidUnload {

    [super viewDidUnload];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (UIDeviceOrientationIsLandscape(toInterfaceOrientation)) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"headeryatay.png"]  forBarMetrics:UIBarMetricsDefault];
        [self.tabBarController.tabBar setBackgroundImage:[UIImage imageNamed:@"tabbaryatay.png"]];
    }
    else if (UIDeviceOrientationIsPortrait(toInterfaceOrientation)) {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"]  forBarMetrics:UIBarMetricsDefault];
    }
}

-(void)configureCell:(WpPostsCell*)cell params:(Post *) n {
    [(WpPostsCell*)cell configureCellWithPost:n];
}

//-(void)configureSliderCell:(SliderCV*)cell withParams:(NSArray *) arr andDelegate:(id) delegate {
//    [(SliderCV*)cell update:arr andDelegate:delegate];
//}

- (void)reloadTableViewDataSource{
    
    self.pageCount = 1;
    [self.dataArr removeAllObjects];
    [self.tableView reloadData];
    self._reloading = YES;
    
}

- (void)doneLoadingTableViewData{
    
    //  model should call this when its done loading
    self._reloading = NO;
    [self._refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	[self._refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[self._refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
    self.tableView.hidden = YES;
    [self showIndicators];
    self.willContinue = YES;
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return self._reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}

-(UITableViewCell*)cellforIndex:(NSIndexPath*)indexPath withReuse:(NSString*)reuse{
    return [[WpPostsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuse] ;
}






-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 120;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    [GlobalData sharedData].posts = self.dataArr;
    WpPostsViewController *vc = [[WpPostsViewController alloc] init];
//    vc.token = self.token;
//    vc.richMediaToken = self.richMediaToken;
    self.navigationItem.title = @"Geri";
    [self.navigationController pushViewController:vc animated:YES];
    [vc loadPost:[self.dataArr objectAtIndex:(indexPath.row)]];
    
    
}

- (void) tappedOnSlider:(int) index {
    NSLog(@"index: %d", index);
    
    WpPostsViewController *vc = [[WpPostsViewController alloc] init];

    self.navigationItem.title = @"Geri";
    [self.navigationController pushViewController:vc animated:YES];
    [vc loadPost:[self.dataArr objectAtIndex:index]];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return NO;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSString * reuse = [NSString stringWithFormat:@"Cell_%d", indexPath.row];
    static NSString *reuse = @"GenericCell";
    
    WpPostsCell * cell = [tableView dequeueReusableCellWithIdentifier:reuse];
    
    if ( cell == nil) {
        cell = (WpPostsCell *) [self cellforIndex:indexPath withReuse:(NSString*)reuse];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    Post *n = [self.dataArr objectAtIndex:(indexPath.row)];
    [self configureCell:cell params:n];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 46.0)];
    
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(19.0, 0.0, 300.0, 46.0)];
	UIColor * bgColor = TABLEVIEW_HEADER_BACKGROUND_COLOR;
    UIColor *textColor = TABLEVIEW_HEADER_TITLE_COLOR;
    header.backgroundColor = bgColor;
    label.backgroundColor= [UIColor clearColor];
	label.textColor = textColor;
	label.font = TABLEVIEW_HEADER_TITLE_FONT;
    [header addSubview:label];
	
     label.text = self.headerTitle;

    return header;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if ([self isKindOfClass:[HomeViewController class]]){
//        return 0.0;
//    }
//	else
        return 46.0;
}

-(void) hideIndicators {
    self.loadingView.hidden = YES;
    indicator.hidden = YES;
    //indicatorNav.hidden = YES;
    [indicator stopAnimating];
    //[indicatorNav stopAnimating];
}

-(void) showIndicators {
    self.loadingView.hidden = NO;
    indicator.hidden = NO;
    //indicatorNav.hidden = NO;
    [indicator startAnimating];
    //[indicatorNav startAnimating];
    
}

@end
