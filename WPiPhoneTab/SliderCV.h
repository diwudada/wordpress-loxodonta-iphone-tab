//
//  DealCellView.h
//  PSM
//
//  Created by Mustafa Çivik on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "DDPageControl.h"

@protocol SlideViewDelegate<NSObject>

- (void) tappedOnSlider:(int) index;

@optional


@end

@interface SlideView : UIView <SlideViewDelegate>	{
    
	id delegate;
    int index;
	UIImageView *imageView;
    UIImageView *selectedImageView;
    UILabel *lblTitle;
    BOOL isSelected;
    BOOL isSelectable;
}

@property (nonatomic, retain) id delegate;
@property int index;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UIImageView *selectedImageView;
@property (nonatomic, retain) UILabel *lblTitle;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) BOOL isSelectable;

@end

@interface SliderCV : UIView <UIScrollViewDelegate> { //<SDWebImageManagerDelegate> {

}

@property (assign) id delegate;
@property (retain, nonatomic) NSMutableArray *dataArr;
@property (retain, nonatomic) IBOutlet UIScrollView *scroll;
@property (retain, nonatomic) DDPageControl *pControl;
@property (nonatomic, retain) NSTimer *timer;
- (void) update:(NSArray *) arr andDelegate:(id) dlg;
///- (void) update:(NSArray *) arr;
- (IBAction)prevNext:(id)sender;
-(void) startTimer ;
@end
