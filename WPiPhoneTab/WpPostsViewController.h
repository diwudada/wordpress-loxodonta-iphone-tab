//
//  WpPostsViewController.h
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Wordpress.h"
#import "WpSettings.h"

@interface WpPostsViewController : UIViewController 
@property (nonatomic, retain) IBOutlet UIWebView  * webView;

-(void)loadPost:(Post *) p;
@end
