//
//  WPHomeViewController.h
//  WPiPhoneTab
//
//  Created by Mehmet Can Yavuz on 14.05.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WpBaseViewController.h"
@interface WPHomeViewController : WpBaseViewController
{
    
}
@property (nonatomic, retain) IBOutlet SliderCV *slider;
@end
