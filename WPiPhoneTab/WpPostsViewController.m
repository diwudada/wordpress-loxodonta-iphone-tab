//
//  WpPostsViewController.m
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "WpPostsViewController.h"
#import "AppDelegate.h"
@interface WpPostsViewController ()

@end

@implementation WpPostsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadPost:(Post *)p{


     NSString *imageHeader = @"";
    
    NSString* htmlStyle = @"<meta name=\"viewport\" id=\"view\" content=\"width=320px, minimum-scale=1.0, maximum-scale=1.0\"><style type=\"text/css\">div.body {margin-top:20px; margin-bottom:20px;  font-family: Georgia, serif} div.title {font-family: Helvetica, Verdana, serif; color:black; font-size: 20px; font-weight: bold;} div.author {color: gray; font-size: 12px; font-weight: italic} div.content {color: black} img {margin-top:10px; margin-bottom:10px; margin-right:10px; margin-left:10px;} </style>";
    
    NSString* htmlString = [NSString stringWithFormat:@"<html><head>%@</head><body>%@<div class=\"body\"><div class=\"title\">%@</div><div class=\"author\"> by %@ </div><div class=\"content\">%@</div></div></body></html>",
                            htmlStyle,
                            imageHeader,
                            p.title,
                            p.author,
                            p.content];
    
    //self.webView.scalesPageToFit=YES;
    [self.webView loadHTMLString:htmlString baseURL:nil];
    NSLog(@"%@",htmlString);
    
    
    
}


@end
