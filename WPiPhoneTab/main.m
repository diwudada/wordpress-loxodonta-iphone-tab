//
//  main.m
//  WPiPhoneTab
//
//  Created by Mehmet Can Yavuz on 14.05.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
