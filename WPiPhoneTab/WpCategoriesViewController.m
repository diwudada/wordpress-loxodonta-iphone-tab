//
//  WpCategoriesViewController.m
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "WpCategoriesViewController.h"
#import "WpSettings.h"
#import "AppDelegate.h"
@interface WpCategoriesViewController ()

@end

@implementation WpCategoriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.dataArr = [[NSMutableArray alloc] init];
    
    if(self._refreshHeaderView == nil){
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
        view.delegate = self;
        [self.tableView addSubview:view];
        self._refreshHeaderView = view;
    }
    [self._refreshHeaderView refreshLastUpdatedDate];
    [self reloadTableViewDataSource];
    self.loadingView.hidden= NO;
}

-(void)viewWillAppear:(BOOL)animated{
//    AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
//    del.stackViewController.delegate = self;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
    [super reloadTableViewDataSource];
    [self getPosts];
}

-(void) getPosts {
    self._reloading = YES;
//    indicatorNav.hidden = NO;
//    [indicatorNav startAnimating];
    if([self.dataArr count] ==0){
        self.page = 1;
     
    }
    
    GetCategoryPostsRequest * request = [[GetCategoryPostsRequest alloc] initWithDelegate:self];
    NSString * session = [[RequestManager sharedManager] globalSession];
    request.session = session;
    request.page = self.page;
    request.categorySlug=self.category;
    request.blogName = BLOGNAME;
    request.successBlock = ^{
        GetCategoryPostsResponse *response = [[GetCategoryPostsResponse alloc] init];
        response = (GetCategoryPostsResponse * )request.response;
        self.pageCount = response.pageCount;
            [self.dataArr addObjectsFromArray:response.posts];

            
            NSLog(@"success");
            NSLog(@"%@",response);
            self._reloading = NO;
            self.tableView.hidden = NO;
            [self hideIndicators];
            [self._refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
            
            [self.tableView reloadData];
        
    };
    
    request.failureBlock = ^{
        NSLog(@"fail");
        // InnerFault * response = (InnerFault * )request.response;
        
        [self hideIndicators];
        self._reloading = NO;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Bağlantı Hatası" message:@"Lütfen tekrar deneyiniz" delegate:self cancelButtonTitle:@"Vazgeç" otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Tamam"];
        [alert show];
        
    };
    
    [request run];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if(indexPath.row  == self.dataArr.count-1 ){
        if (self.page < self.pageCount && !self._reloading ) {
            self.page += 1;
            [self getPosts];
        }
    }
    
}

//- (BOOL)stackViewController:(MTStackViewController *)stackViewController willRevealLeftViewController:(UIViewController * ) viewController{
//    return YES;
//}
@end
