//
//  WpPostsCell.h
//  WpTest
//
//  Created by Mehmet Can Yavuz on 19.03.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Wordpress.h"

@interface WpPostsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * description;
@property (nonatomic, weak) IBOutlet UIImageView * thumbnail;
@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel * category;
@property (nonatomic, weak) IBOutlet UIImageView * categoryIcon;
@property (nonatomic, weak) IBOutlet UILabel *dateCreated;
@property (nonatomic, weak) IBOutlet UILabel *author;

-(void) configureCellWithPost:(Post *) p;

@end
